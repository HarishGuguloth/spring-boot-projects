package com.antra.demo.entities;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="STUDENTS")
public class StudentEntity 
{
	@Id
	private Integer StudentId;
	
	@Column(length=20)
	private String studentname;
	
	@Column(length=20)
	private String Studentbranch;
	
	private Collection<TeacherEntity> teachers;

	public Integer getStudentId() {
		return StudentId;
	}

	public void setStudentId(Integer studentId) {
		StudentId = studentId;
	}

	public String getStudentname() {
		return studentname;
	}

	public void setStudentname(String studentname) {
		this.studentname = studentname;
	}

	public String getStudentbranch() {
		return Studentbranch;
	}

	public void setStudentbranch(String studentbranch) {
		Studentbranch = studentbranch;
	}

	public Collection<TeacherEntity> getTeachers() {
		return teachers;
	}

	public void setTeachers(Collection<TeacherEntity> teachers) {
		this.teachers = teachers;
	}

	@Override
	public String toString() {
		return "StudentEntity [StudentId=" + StudentId + ", studentname=" + studentname + ", Studentbranch="
				+ Studentbranch + ", teachers=" + teachers + "]";
	}
	

}
