package com.antra.demo.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TEACHERS")
public class TeacherEntity 
{
	@Id
	private Integer teacherId;
	
	@Column(length=20)
	private String teacherName;

	public Integer getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Integer teacherId) {
		this.teacherId = teacherId;
	}

	public String getTeacherName() {
		return teacherName;
	}

	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	@Override
	public String toString() {
		return "TeacherEntity [teacherId=" + teacherId + ", teacherName=" + teacherName + "]";
	}
	

	
	
	
	
}
