package com.antra.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.antra.demo.entities.TeacherEntity;

public interface TeachersRepository extends JpaRepository<TeacherEntity, Integer> {

}
