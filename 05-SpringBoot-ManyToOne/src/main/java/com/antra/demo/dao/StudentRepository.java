package com.antra.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.antra.demo.entities.StudentEntity;

public interface StudentRepository extends JpaRepository<StudentEntity, Integer> 
{
	

}
