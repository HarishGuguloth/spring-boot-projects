package com.antra.demo.runner;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.antra.demo.dao.ProductEntityRepository;
import com.antra.demo.entity.ProductEntity;

@Component
public class MyCommandLineRunner implements CommandLineRunner {
	
	@Autowired
	ProductEntityRepository repository;

	@Override
	public void run(String... args) throws Exception 
	{
		//inserting records
		/*List<ProductEntity> lst=ProductHelper.getProducts();
		for(ProductEntity p:lst)
		{
			repository.save(p);
		}*/
		
		
		//loading one entity only
		/*
		Optional<ProductEntity> pp=repository.findById(222);
		if(pp.isPresent())
		{
		 ProductEntity p=pp.get();
		 System.out.println(p);
		}*/
		
		
		//loading all entities
		
		/*List<ProductEntity> lst=repository.findAll();
		for(ProductEntity p:lst)
		{
			System.out.println(p);
		}*/
		
		//deleting one entity
	/*	repository.deleteById(111);
		List<ProductEntity> lst=repository.findAll();
		for(ProductEntity p:lst)
		{
			System.out.println(p);
		}
		//updating entity
		ProductEntity p=new ProductEntity();
		
		*/
	/*List<ProductEntity> lst=	repository.findByProductName("watch");
	lst.forEach(System.out::println);
		*/
	/*	List<ProductEntity> lst=repository.fetchByProductNameFromDB("TV");
		lst.forEach(System.out::println);
		*/
		List<ProductEntity> lst=repository.fetchRecordsUsingProductName("phone");
		lst.forEach(System.out::println);

	}

}
