package com.antra.demo.helper;

import java.util.ArrayList;
import java.util.List;

import com.antra.demo.entity.ProductEntity;

public class ProductHelper 
{
	public static List<ProductEntity> getProducts()
	{
		List<ProductEntity> lst=new ArrayList<>();
		ProductEntity p1=new ProductEntity();
		p1.setProductId(111);
		p1.setProductName("watch");
		p1.setProductPrice(1500.0);
		ProductEntity p2=new ProductEntity();
		p2.setProductId(222);
		p2.setProductName("phone");
		p2.setProductPrice(10000.0);
		lst.add(p1);
		lst.add(p2);
		return lst;
		
	}

}
