package com.antra.demo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.antra.demo.entity.ProductEntity;


@Repository
public interface ProductEntityRepository extends JpaRepository<ProductEntity,Integer> 
{
	//List<ProductEntity> findByProductName(String productname);

	
	
	    /* @Query("select p from ProductEntity p where p.productName like concat('%',?1,'%')")
	     List<ProductEntity> fetchByProductNameFromDB(String pname);
	*/
	
	@Query(value="select * from products where product_name like concat('%',?,'%')",nativeQuery=true)
	List<ProductEntity> fetchRecordsUsingProductName(String p);
}
