package com.antra.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.antra.demo.entities.OrdersEntity;
@Repository
public interface OrdersRepository extends JpaRepository<OrdersEntity, Integer> 
{
	

}
