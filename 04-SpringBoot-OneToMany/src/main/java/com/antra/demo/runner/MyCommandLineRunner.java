package com.antra.demo.runner;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.antra.demo.dao.CustomerRepository;
import com.antra.demo.dao.OrdersRepository;
import com.antra.demo.entities.CustomerEntity;
import com.antra.demo.helper.CustomerHelper;
@Component
public class MyCommandLineRunner implements CommandLineRunner 
{
	@Autowired
	CustomerRepository cust;
	
	
	@Autowired
	OrdersRepository order;

	@Override
	public void run(String... args) throws Exception 
	{
	List<CustomerEntity> lst=CustomerHelper.addCustomers();
	for(CustomerEntity ce:lst)
	{
		cust.save(ce);
		
	}
		
				//cust.save(lst);

	}

}
