package com.antra.demo.helper;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.antra.demo.entities.OrdersEntity;

public class OrdersHelper 
{
	public static List<OrdersEntity> addOrders()
	{
		List<OrdersEntity> lst=new ArrayList<>();
		OrdersEntity o1=new OrdersEntity();
		o1.setOrderId(912);
		o1.setOrderName("shirt");
		o1.setDateOfOrder(LocalDate.now());
		
		OrdersEntity o2=new OrdersEntity();
		o2.setOrderId(108);
		o2.setOrderName("ipad");
		o2.setDateOfOrder(LocalDate.of(2022, 2, 18));
		lst.add(o1); lst.add(o2);
		return lst;
		
		
	}

}
