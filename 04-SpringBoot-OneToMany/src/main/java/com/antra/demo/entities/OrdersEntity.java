package com.antra.demo.entities;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ORDERS")

public class OrdersEntity 
{
	@Id
	private Integer orderId;
	@Column(length=20)
	private String orderName;
	
	private LocalDate dateOfOrder;

	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public String getOrderName() {
		return orderName;
	}

	public void setOrderName(String orderName) {
		this.orderName = orderName;
	}

	public LocalDate getDateOfOrder() {
		return dateOfOrder;
	}

	public void setDateOfOrder(LocalDate dateOfOrder) {
		this.dateOfOrder = dateOfOrder;
	}

	@Override
	public String toString() {
		return "OrdersEntity [orderId=" + orderId + ", orderName=" + orderName + ", dateOfOrder=" + dateOfOrder + "]";
	}
	
	
	
	

}
