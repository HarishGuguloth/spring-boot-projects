package com.antra.demo.entities;

import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="CUSTOMERS")
public class CustomerEntity 
{
	@Id
	private Integer custId;
	
	@Column(length=20)
	private String custName;
	
	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name="custId_FK")
	 private Collection<OrdersEntity> orders;

	public Integer getCustId() {
		return custId;
	}

	public void setCustId(Integer custId) {
		this.custId = custId;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public Collection<OrdersEntity> getOrders() {
		return orders;
	}

	public void setOrders(Collection<OrdersEntity> orders) {
		this.orders = orders;
	}

	@Override
	public String toString() {
		return "CustomerEntity [custId=" + custId + ", custName=" + custName + ", orders=" + orders + "]";
	}
	
	

}
