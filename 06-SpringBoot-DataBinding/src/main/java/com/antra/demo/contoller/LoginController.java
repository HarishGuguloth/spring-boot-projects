package com.antra.demo.contoller;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.antra.demo.bean.LoginBeanClass;

@Controller
public class LoginController 
{
	@GetMapping(value="login")
	public String getLoginPageMethod(Model model)
	{
		LoginBeanClass bean=new LoginBeanClass();
		model.addAttribute("bean",bean);
		
		return "Login";
		
	}
	@PostMapping(value="check")
	public String checkingLoginDetails(@Valid @ModelAttribute(value="bean") LoginBeanClass bean,BindingResult br,Model model)
	{
		if(br.hasErrors())
		{
			return "Login";
		}
		else
		{
		  model.addAttribute("message","bad credentials....");
		  model.addAttribute("user", bean.getUsername());
		  String uname=bean.getUsername();
		  String pword=bean.getPassword();
		  if(uname.equals("harish") && pword.equals("Harish@1627"))
		  {
			return "Success";
		  }
		  else
		  {
			return "Login";
		  }
		}
		
		
	}
	

}
