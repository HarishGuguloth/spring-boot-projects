package com.antra.demo.dao.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.antra.demo.dao.EmployeeDAO;

@Repository

public class EmployeeDAOImpl implements EmployeeDAO {
	
	@Autowired
	JdbcTemplate jdbcTemplate;

	@Override
	public int addEmployee(int empId, String ename, double salary, int deptNo)
	{
		 int i=jdbcTemplate.update("insert into employee values(?,?,?,?)", empId,ename,salary,deptNo);
		 return i;
		
		
	}
	
	
	public Map fetchRecords(int empid)
	{
		
		Map map=jdbcTemplate.queryForMap("select * from employee where empId=?",empid);
		return map;
		
	}

}
