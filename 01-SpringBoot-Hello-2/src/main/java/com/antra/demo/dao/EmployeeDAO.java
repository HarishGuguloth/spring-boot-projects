package com.antra.demo.dao;

import java.util.Map;

public interface EmployeeDAO {
	
	int addEmployee(int empId,String ename,double salary,int deptNo);
	
	
	
	Map fetchRecords(int empId);

}
