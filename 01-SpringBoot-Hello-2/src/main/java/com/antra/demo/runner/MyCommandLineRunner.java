package com.antra.demo.runner;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.antra.demo.dao.impl.EmployeeDAOImpl;

@Component

public class MyCommandLineRunner implements CommandLineRunner {
	@Autowired
	EmployeeDAOImpl employeeDAOImpl;

	@Override
	public void run(String... args) throws Exception {
		/*int i=employeeDAOImpl.addEmployee(9, "nagarjuna", 30000.0, 20);
		if(i==1)
		{
			System.out.println("record inserted successfully.....");
		}
		else
		{
			System.out.println("record not-inserted .....");
			
		}*/
		
		
		Map map=employeeDAOImpl.fetchRecords(123);
		System.out.println(map);

	}

}
