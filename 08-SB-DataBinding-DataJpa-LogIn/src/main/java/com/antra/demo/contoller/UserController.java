package com.antra.demo.contoller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.antra.demo.bean.UserBean;

@Controller
public class UserController 
{
	
	@GetMapping(value="index")
	public String indexHandler()
	{
		return "Index";
	}
	@GetMapping(value="register")
	public String registrationHandler(Model model)
	{
		UserBean ub=new UserBean();
		model.addAttribute("user", ub);
		
		return "Registration";
	}
	
	@PostMapping(value="newRegister")
	public String checkRegistration(@Valid @ModelAttribute(value="user") UserBean ub,BindingResult br)
	{
		if(br.hasErrors())
		{
			return "Registration";
		}
		else
		{
		return "RegistrationSuccess";
		}
		
	}
	
	
	@GetMapping(value="login")
	public String loginHandler()
	{
		
		return "Login";
		
	}
	@PostMapping(value="check")
	public String checkLogin(HttpServletRequest req,HttpServletResponse res)
	{
		
		
		if(req.getParameter("username").equals("rajarani") && req.getParameter("password").equals("Harish@1627"))
		{
			return "Success";
		}
		else
		{
		    return "Fail";
		}
		
		
	}

}
