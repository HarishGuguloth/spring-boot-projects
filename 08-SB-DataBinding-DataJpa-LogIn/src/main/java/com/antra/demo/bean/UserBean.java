package com.antra.demo.bean;

import java.util.Date;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

public class UserBean 
{
	@NotEmpty
	@Size(min=8,max=12)
	private String username;
	@NotEmpty
	@Pattern(regexp="^[A-Z][a-z]*@{1}[0-9]{4}")
	 private String password;
	@NotEmpty
	 private String fname;
	@NotEmpty
	 private String lname;
	@NotEmpty
	 @DateTimeFormat(pattern = "dd-MM-yyyy")
	 private Date date;
	@NotEmpty
	 private String email;
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	 

}
