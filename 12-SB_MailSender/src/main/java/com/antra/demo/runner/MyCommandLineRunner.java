package com.antra.demo.runner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.antra.demo.service.impl.MailServiceImpl;

@Component
public class MyCommandLineRunner implements CommandLineRunner 
{
	@Autowired
	MailServiceImpl service;

	@Override
	public void run(String... args) throws Exception 
	{
		//service.sendSimpleMail("nivaas2630@gmail.com", "this is the text of the mail", "this the mail subject");
		
		service.SendMailWithAttahement("nivaas2630@gmail.com", "this is the text of the mail ", "this is the mail subject","C:\\Users\\Santosh\\Downloads\\download1.jpg");
		

	}

}
