package com.antra.demo.service;

public interface IMailService 
{
	
	void sendSimpleMail(String toMail,String text,String body);
	
	void SendMailWithAttahement(String toMail,String text,String body,String attachement);

}
