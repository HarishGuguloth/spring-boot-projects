package com.antra.demo.service.impl;

import java.io.File;

import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.antra.demo.service.IMailService;

@Service
public class MailServiceImpl implements IMailService 
{
	@Autowired
	JavaMailSender mailSender;

	@Override
	public void sendSimpleMail(String toMail, String text, String body) 
	{
		SimpleMailMessage msg=new SimpleMailMessage();
		
		msg.setFrom("gugulothharishharsha@gmail.com");
		msg.setTo(toMail);
		msg.setText(text);
		msg.setSubject(body);
		mailSender.send(msg);
		System.out.println("mail sended.....");
	}

	@Override
	public void SendMailWithAttahement(String toMail, String text, String body, String attachement)
	{
		try
		{
		MimeMessage msg=mailSender.createMimeMessage();
		MimeMessageHelper msgHelper=new MimeMessageHelper(msg, true);
		msgHelper.setFrom("gugulothharishharsha@gmail.com");
		msgHelper.setTo(toMail);
		msgHelper.setText(text);
		msgHelper.setSubject(body);
		FileSystemResource res=new FileSystemResource(new File(attachement));
		msgHelper.addAttachment(res.getFilename(), res);
		mailSender.send(msg);
		System.out.println("mail with attach sended...");
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
		

	}

}
