package com.antra.demo.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.antra.demo.service.ServiceInterface;
@Service
public class ServiceImpl implements ServiceInterface 
{
	List<Integer> lst=new ArrayList<>();
	public  ServiceImpl()
	{
		lst.add(1);
		lst.add(23);
		lst.add(45);
		lst.add(29);
		
	}
	

	@Override
	public List<Integer> getAllValues() 
	{
		
	
		return lst;
	}

	@Override
	public Integer getParticularValue(int index) {
		Integer i=lst.get(index);
		
		return i;
	}

	@Override
	public boolean addValueToList(int value)
	{
		boolean flag=lst.add(value);
		if(flag==true)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	@Override
	public boolean updatevalueInList(int index, Integer value) 
	{
		
		Integer i=lst.get(index);
		
		i=value;
		boolean b=lst.add(i);
		if(b==true)
		{
			return true;
		}
		else
		{
		
		
		return false;
		}
	}

	@Override
	public boolean deleteValueFromList(int index) {
		Integer i=lst.remove(index);
		if(i!=null)
		{
			return true;
		}
		else
		{
		return false;
		}
	}

}
