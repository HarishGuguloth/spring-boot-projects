package com.antra.demo.service;

import java.util.List;

public interface ServiceInterface 
{
	List<Integer> getAllValues();
	
	Integer  getParticularValue(int index);
	
	boolean addValueToList(int index);
	boolean updatevalueInList(int index,Integer value);
	boolean deleteValueFromList(int index);
	
	

}
