package com.antra.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import com.antra.demo.service.impl.ServiceImpl;

@RestController
public class RestControllerClass 
{
	@Autowired
	ServiceImpl service;
	@GetMapping(value="/list")
	public ResponseEntity<?> getList()
	{
		List<Integer> li=service.getAllValues();
		ResponseEntity<Integer> re=new ResponseEntity(li,HttpStatus.OK);
		return re;
	}
	@GetMapping(value="/list/{value}")
	public ResponseEntity<?> getParticularValue(@PathVariable int value)
	{
		Integer i=service.getParticularValue(value);
		ResponseEntity<Integer> re=new ResponseEntity(i,HttpStatus.OK);
		return re;
		
	}
	@PostMapping(value="/list/add/{value}")
	public ResponseEntity<?> addvalue(@PathVariable Integer value)
	{
		boolean b=service.addValueToList(value);
		if(b==true)
		{
			ResponseEntity<String> re=new ResponseEntity("value added successfully to list..",HttpStatus.OK);
			return re;
			
		}
		else
		{
			ResponseEntity<String> re=new ResponseEntity("value not adde to list..",HttpStatus.BAD_REQUEST);
			return re;

			
		}
		
	}
	@PutMapping(value="/list/update/{old}/{newvalue}")
	public ResponseEntity<?> updateValuesInList(@PathVariable Integer old,Integer newvalue)
	{
		boolean b=service.updatevalueInList(old, newvalue);
		if(b==true)
		{
			ResponseEntity<String> re=new ResponseEntity("value updated successfully to list..",HttpStatus.OK);
			return re;
			
		}
		else
		{
			ResponseEntity<String> re=new ResponseEntity("value not updated ",HttpStatus.BAD_REQUEST);
			return re;

			
		}
		
	}
	@DeleteMapping(value="/list/delete/{value}")
	public ResponseEntity<?> deleteValueFromList(@PathVariable int value)
	{
		boolean b=service.deleteValueFromList(value);
		if(b==true)
		{
			ResponseEntity<String> re=new ResponseEntity("value deleted successfully from list..",HttpStatus.OK);
			return re;
			
		}
		else
		{
			ResponseEntity<String> re=new ResponseEntity("value not deleted.. ",HttpStatus.BAD_REQUEST);
			return re;

			
		}
		
	}
	

	
	
	
}
