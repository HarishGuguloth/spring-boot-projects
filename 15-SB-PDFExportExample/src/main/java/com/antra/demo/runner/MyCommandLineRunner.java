package com.antra.demo.runner;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.antra.demo.service.impl.PDFExporterServiceImpl;


@Component
public class MyCommandLineRunner implements CommandLineRunner
{
	@Autowired
	PDFExporterServiceImpl  service;


	@Override
	public void run(String... args) throws Exception 
	{
		//res.setContentType("application/pdf");
		service.exportPDF();
		
	

	}

}
