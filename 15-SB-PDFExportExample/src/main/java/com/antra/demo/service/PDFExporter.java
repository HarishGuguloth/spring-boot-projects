package com.antra.demo.service;

import javax.servlet.http.HttpServletResponse;

public interface PDFExporter 
{
	void exportPDF() throws Exception;

}
