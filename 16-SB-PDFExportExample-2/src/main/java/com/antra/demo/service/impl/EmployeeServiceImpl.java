package com.antra.demo.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.antra.demo.dao.EmployeeRepository;
import com.antra.demo.dto.Employee;
import com.antra.demo.entity.EmployeeEntity;
import com.antra.demo.service.EmployeeService;

@Service
public class EmployeeServiceImpl implements EmployeeService 
{
	@Autowired
	EmployeeRepository repo;

	@Override
	public List<Employee> getAllEmployees() 
	{
		List<EmployeeEntity> lst=repo.findAll();
		List<Employee> lstEmp=new ArrayList<>();
		lst.stream().forEach(e-> {
			Employee ee=new Employee();
			BeanUtils.copyProperties(e, ee);
			lstEmp.add(ee);
		});
		
		//BeanUtils.copyProperties(lst, lstEmp);
		System.out.println("done");
		
		return lstEmp;
	}

}
