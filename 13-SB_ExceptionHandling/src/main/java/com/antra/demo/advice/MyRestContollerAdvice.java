package com.antra.demo.advice;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.antra.demo.exception.CustomerNotExitException;

@RestControllerAdvice
public class MyRestContollerAdvice 
{
	@ExceptionHandler(CustomerNotExitException.class)
	public String exceptionHandler1(CustomerNotExitException e)
	{
		return e.getMessage();
		
	}

}
