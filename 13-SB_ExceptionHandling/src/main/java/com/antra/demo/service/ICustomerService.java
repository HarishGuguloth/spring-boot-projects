package com.antra.demo.service;

import com.antra.demo.dto.Customer;

public interface ICustomerService 
{
	boolean addCustomer(Customer c);
	
	Customer findCustomerById(Integer custId)throws Exception;
	

}
