package com.antra.demo.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.antra.demo.dto.Customer;
import com.antra.demo.exception.CustomerNotExitException;
import com.antra.demo.service.impl.CustomerServiceImpl;

@RestController
public class CustomerRestController
{
	@Autowired
	CustomerServiceImpl service;
	@PostMapping(value="/add")
	public ResponseEntity<?> addCustomer( @RequestBody Customer cust)
	{
		boolean b=service.addCustomer(cust);
		if(b)
		{
			ResponseEntity<String> res=new ResponseEntity<>("Added",HttpStatus.CREATED);
			return res;
		}
		else
		{
			ResponseEntity<String> res=new ResponseEntity<>("Not Added",HttpStatus.BAD_REQUEST);
			return res;
			
		}
		
		
	}
	@GetMapping(value="/get/{cid}")
	
	public ResponseEntity<?> getCustomerBasedOnId(@PathVariable Integer cid) throws Exception
	{
	
		Customer c=service.findCustomerById(cid);
		ResponseEntity<Customer> res=new ResponseEntity<Customer>(c,HttpStatus.OK);
		return res;
		
		
		
	}
	

}
