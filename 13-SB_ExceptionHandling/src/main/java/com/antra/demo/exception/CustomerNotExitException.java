package com.antra.demo.exception;

public class CustomerNotExitException extends RuntimeException 
{
	public CustomerNotExitException(String msg)
	{
		super(msg);
	}

}
